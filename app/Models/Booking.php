<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;

    protected $table = 'bookings';

    protected $fillable = [
        'user_id', 'room_id', 'check_in', 'check_out', 'total', 'status', 'payment_status', 'note', 'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function room()
    {
        return $this->belongsTo(Room::class);
    }

    public function scopeSearch($query){
        if($date = request()->date){
            $query->whereDay('check_in', '=', $date);
        }elseif($month = request()->month){
            $query->whereMonth('check_in', '=', $month);
        }elseif($year = request()->year){
            $query->whereYear('check_in', '=', $year);
        }
        return $query;
    }

    public function scopeWithUser($query){
        if($key = request()->key){
            return $query->whereHas('user', function($q) use($key) {
                $q->where('name', 'like', '%' . $key . '%');
            });
        }
    }

    public function scopeWithManager($query){
        if($key = request()->key){
            return $query->orWhereHas('room', function($q) use($key) {
                $q->where('name', 'like', '%' . $key . '%');
            });
        }
    }

    public function scopeWithSortByDate($query){
        if(request()->check_in && request()->check_out){
            $check_in = request()->check_in;
            $check_out = request()->check_out;
            $query->whereBetween('check_in', [$check_in, $check_out]);
        }
        return $query;
    }

    
    
}
