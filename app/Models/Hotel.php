<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    use HasFactory;

    public function scopeSearch($query){
        if($key = request()->key){
            $query->where('name', 'like', '%'.$key.'%');
        }
        return $query;
    }
}
