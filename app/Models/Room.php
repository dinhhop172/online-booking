<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    use HasFactory;

    protected $table = 'rooms';

    protected $fillable = [
        'user_id', 'hotel_id', 'name', 'img', 'background', 'price', 'status',
    ];

    public function hotel()
    {
        return $this->belongsTo(Hotel::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }

    public function scopeWithSearch($query)
    {
        if(request()->key) {
            $key = request()->get('key');
            $query->where('name', 'like', '%' . $key . '%');
        }
        return $query;
    }
}
