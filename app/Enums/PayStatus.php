<?php

namespace App\Enums;

use BenSampo\Enum\Enum;


final class PayStatus extends Enum
{
    const UNPAID =   0;
    const PAID =   1;
}
