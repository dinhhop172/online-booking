<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class BookingStatus extends Enum
{
    const SCHEDULE = 1;
    const PAID = 2;
    const DONE = 3;
    const CANCEL = 4;
}
