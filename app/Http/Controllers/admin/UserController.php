<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = User::all();
        return view('admin.users.index',compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.form');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $checkmail = User::where('email',$data['email'])->count();
        if ($checkmail == 0) {
            $user = new User();
            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->status = '1';
            $user->password = Hash::make($data['password']);
            // $user->name = $data['repassword'];
            $user->role = $data['role'];
            $user->save();
            $list = User::all();
            Mail::send('mails.info_user', array('name'=>$data['name'],'email'=>$data['email'], 'content'=>'Chào mừng bạn đã đăng ký, mật khẩu ứng dụng của bạn là:'.$data['password']), function($message)use ($request){
                $message->to($request->email, 'Nguyễn Quang Đức')->subject('Nguyễn Quang Đức!');
            });
            // Session::flash('flash_message', 'Send message successfully!');
            return view('admin.users.index',compact('list'));
        }else{
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('admin.users.detail',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        //dd($user);
         return view('admin.users.form',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $user = User::find($id);
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        // $user->name = $data['repassword'];
        $user->role = $data['role'];
        $user->save();
        $list = User::all();
        return view('admin.users.index',compact('list'));
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user  = User::find($id)->delete();
        $list = User::all();
        return view('admin.users.index',compact('list'));
        // return redirect()->back();
    }
    public function change_active($id){
        $user = User::find($id);
        if ($user->status == 0) {
            $user->status =1;
        }else{
            $user->status =0;
        }
        $user->save();
        $list = User::all();
        return view('admin.users.index',compact('list'));
    }

}
