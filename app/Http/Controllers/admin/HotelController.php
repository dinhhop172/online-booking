<?php

namespace App\Http\Controllers\admin;

use App\Enums\BookingStatus;
use App\Http\Controllers\Controller;
use App\Models\Hotel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hotels = Hotel::search()->paginate(5);
        return view('admin.hotels.index',compact('hotels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $age = array("H1"=>"h1.png","H2"=>"h2.png","H3"=>"h3.png","H4"=>"h4.png","H5"=>"h5.png","H6"=>"h6.png","H7"=>"h7.png","H8"=>"h8.png","H9"=>"h9.png","H10"=>"h10.png",);
        // dd(json_encode($age));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detailHotel = DB::table('categories')
            ->join('category_hotel', 'categories.id', '=', 'category_hotel.category_id')
            ->join('hotels', 'category_hotel.hotel_id', '=', 'hotels.id')
            ->join('users', 'hotels.user_id', '=', 'users.id')
            ->select('categories.name as view','users.name as name')
            ->where('hotels.id', $id)
            ->get();
        $hotel = Hotel::find($id);
        return view('admin.hotels.detail', compact('hotel', 'detailHotel'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Hotel $hotels)
    {
        return view('admin.hotel.edit',compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hotel $hotels)
    {
        $request->validate([
            'name' => 'required',
            'address' => 'required',
        ]);
        $hotels->update($request->all());
        return redirect()->route('admin.hotel.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hotel = Hotel::find($id);
        $checkBooking = DB::table('bookings')
            ->join('rooms', 'bookings.room_id', '=', 'rooms.id')
            ->join('hotels', 'rooms.hotel_id', '=', 'hotels.id')
            ->select('bookings.status as status')
            ->where('hotels.id', $id)
            ->get();
        if(count($checkBooking) < 1){
            $hotel->delete();
            return redirect()->route('admin.hotel.index')->with('success', 'Delete success');
        }elseif(($checkBooking[0]->status == BookingStatus::PAID)){
            return redirect()->back()->with('error', 'Hotels in use');
        }        

    }
}
