<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ExcelExport;
use App\Exports\ExcelMonth;
use App\Exports\ExcelYear;

class DashBoardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {     
        $revenues = Booking::latest()->search()
            ->join('users', 'bookings.user_id', '=', 'users.id')
            ->join('rooms', 'bookings.room_id', '=', 'rooms.id')
            ->join('hotels', 'rooms.hotel_id', '=', 'hotels.id')
            ->select('bookings.*','users.name as userName', 'rooms.name as roomName','hotels.name as hotelName','users.referen_url as url' )
            ->orderByDesc('bookings.check_in')
            ->paginate(5);

            // dd($revenues);
        $revenueMonth = Booking::select(DB::raw('sum(total) as sumMonth, check_in'))
            ->groupBy('check_in')
            ->orderByDesc('check_in')
            ->limit(6)
            ->get();

        $revenueYear = Booking::select(DB::raw('sum(total) as sumYear, YEAR(check_in) as year'))
            ->groupBy('year')
            ->orderByDesc('year')
            ->limit(5)
            ->get();
            
        return view('admin.dashboards.index',compact('revenues','revenueMonth','revenueYear'));
    }

    public function export_csvBooking(){
        return Excel::download(new ExcelExport, 'RevenueByBooking.csv' );
    }
    public function export_csvMonth(){
        return Excel::download(new ExcelMonth, 'RevenueMonth.csv' );
    }
    public function export_csvYear(){
        return Excel::download(new ExcelYear, 'RevenusYear.csv' );
    }
}
