<?php

namespace App\Exports;

use App\Models\Booking;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExcelMonth implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $revenueMonth = Booking::select(DB::raw("(DATE_FORMAT(check_in, '%m-%Y')) as month, sum(total) as sumMonth"))
            ->groupBy('check_in')
            ->orderByDesc('check_in')
            ->get();

        return $revenueMonth;
    }
    public function headings() :array {
        return ["MONTH", "TURNOVER"];
    }
}
