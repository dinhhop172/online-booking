<?php

namespace App\Exports;

use App\Models\Booking;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExcelExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $revenues = Booking::join('users', 'bookings.user_id', '=', 'users.id')
            ->join('rooms', 'bookings.room_id', '=', 'rooms.id')
            ->join('hotels', 'rooms.hotel_id', '=', 'hotels.id')
            ->select('bookings.id','users.name as userName','hotels.name as hotelName', 'rooms.name as roomName','bookings.check_in','bookings.check_out','bookings.total' )
            ->orderByDesc('bookings.check_in')
            ->get();

        return $revenues;
    }
    public function headings() :array {
        return ["NO", "BOOKING UESR", "HOTEL", "ROOM","CHECK IN", "CHECK OUT", "PRICE"];
    }
    
}
