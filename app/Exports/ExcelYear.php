<?php

namespace App\Exports;

use App\Models\Booking;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExcelYear implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $revenueYear = Booking::select(DB::raw('YEAR(check_in) as year,sum(total) as sumYear'))
            ->groupBy('year')
            ->orderByDesc('year')
            ->get();

        return $revenueYear;
    }
    public function headings() :array {
        return ["YEAR", "TURNOVER"];
    }
}
