@if(auth()->user()->role == \App\Enums\UserRole::ADMIN)
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">ADMIN<sup></sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="{{route('admin.dashboards.index')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        LIST MANAGE
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
            aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-cog"></i>
            <span>Users</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">List Manage</h6>
                <a class="collapse-item" href="{{route('create-user')}}">Create</a>
                <a class="collapse-item" href="{{route('users.index')}}">List Users</a>
            </div>
        </div>
    </li>

    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="" data-toggle="collapse" data-target="#collapseUtilities"
            aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Hotels</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
            data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">List Manage</h6>
                <a class="collapse-item" href="utilities-color.html">Create</a>
                <a class="collapse-item" href="{{route('admin.hotel.index')}}">List Hotels</a>
            </div>
        </div>
    </li>
    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#bookings"
            aria-expanded="true" aria-controls="bookings">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Bookings</span>
        </a>
        <div id="bookings" class="collapse" aria-labelledby="headingUtilities"
            data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">List Manage</h6>
                <a class="collapse-item" href="utilities-color.html">Create</a>
                <a class="collapse-item" href="utilities-border.html">List Bookings</a>
            </div>
        </div>
    </li>
    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#rooms"
            aria-expanded="true" aria-controls="rooms">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Rooms</span>
        </a>
        <div id="rooms" class="collapse" aria-labelledby="headingUtilities"
            data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">List Manage</h6>
                <a class="collapse-item" href="utilities-color.html">Create | Edit</a>
                <a class="collapse-item" href="{{ route('admin.rooms.index') }}">List Rooms</a>
            </div>
        </div>
    </li>
    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#repay"
            aria-expanded="true" aria-controls="repay">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Request Payment</span>
        </a>
        <div id="repay" class="collapse" aria-labelledby="headingUtilities"
            data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">List Manage</h6>
                <a class="collapse-item" href="utilities-color.html">Create | Edit</a>
                <a class="collapse-item" href="utilities-border.html">List Request Payment</a>
            </div>
        </div>
    </li>
    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="" data-toggle="collapse" data-target="#category"
            aria-expanded="true" aria-controls="category">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Categories</span>
        </a>
        <div id="category" class="collapse" aria-labelledby="headingUtilities"
            data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">List Manage</h6>
                <a class="collapse-item" href="utilities-color.html">Edit</a>
                <a class="collapse-item" href="{{ route('admin.categories.index') }}">List Categories</a>
            </div>
        </div>
    </li>
    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#rate"
            aria-expanded="true" aria-controls="rate">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Rates</span>
        </a>
        <div id="rate" class="collapse" aria-labelledby="headingUtilities"
            data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">List Manage</h6>
                {{-- <a class="collapse-item" href="utilities-color.html">Edit</a> --}}
                <a class="collapse-item" href="utilities-border.html">List Rates</a>
            </div>
        </div>
    </li>
    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#profile"
            aria-expanded="true" aria-controls="profile">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Profiles</span>
        </a>
        <div id="profile" class="collapse" aria-labelledby="headingUtilities"
            data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">List Manage</h6>
                <a class="collapse-item" href="utilities-color.html">Edit</a>
                <a class="collapse-item" href="utilities-border.html">List</a>
            </div>
        </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        STATISTICS
    </div>
</ul>
@else
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">MANAGER<sup></sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="{{ route('manager.dashboards.index') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        LIST MANAGE
    </div>

    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#bookings"
            aria-expanded="true" aria-controls="bookings">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Bookings</span>
        </a>
        <div id="bookings" class="collapse" aria-labelledby="headingUtilities"
            data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">List Manage</h6>
                <a class="collapse-item" href="utilities-color.html">Create</a>
                <a class="collapse-item" href="{{ route('manager.bookings.index') }}">List Bookings</a>
            </div>
        </div>
    </li>
    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#rooms"
            aria-expanded="true" aria-controls="rooms">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Rooms</span>
        </a>
        <div id="rooms" class="collapse" aria-labelledby="headingUtilities"
            data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">List Manage</h6>
                <a class="collapse-item" href="utilities-color.html">Create | Edit</a>
                <a class="collapse-item" href="">List Rooms</a>
            </div>
        </div>
    </li>
    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#rate"
            aria-expanded="true" aria-controls="rate">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Rates</span>
        </a>
        <div id="rate" class="collapse" aria-labelledby="headingUtilities"
            data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">List Manage</h6>
                {{-- <a class="collapse-item" href="utilities-color.html">Edit</a> --}}
                <a class="collapse-item" href="utilities-border.html">List Rates</a>
            </div>
        </div>
    </li>
    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#profile"
            aria-expanded="true" aria-controls="profile">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Profiles</span>
        </a>
        <div id="profile" class="collapse" aria-labelledby="headingUtilities"
            data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">List Manage</h6>
                <a class="collapse-item" href="utilities-color.html">Edit</a>
                <a class="collapse-item" href="utilities-border.html">List</a>
            </div>
        </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        STATISTICS
    </div>
</ul>
@endif