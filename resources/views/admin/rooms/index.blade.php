@extends('layouts.admin.master')
@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">LIST ROOMS</h6>
        </div>
        <form method="GET" class="pt-3 d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <div class="input-group">
                <input type="text" name="key" class="form-control bg-light border-0 small" placeholder="Search for..."
                    aria-label="Search" aria-describedby="basic-addon2">
                <div class="input-group-append">
                    <button class="btn btn-primary" type="submit">
                        <i class="fas fa-search fa-sm"></i>
                    </button>
                </div>
            </div>
        </form>
        
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>*</th>
                            <th>Name</th>
                            <th>Image</th>
                            <th>Price</th>
                            <th>Status</th>
                            <th>Acction</th>
                        </tr>
                    </thead>

                    <tbody>
                        @php
                            $i = 1;
                        @endphp
                        @foreach ($rooms as $room)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $room->name }}</td>
                            <td>
                                <img width="100%" height="100px" src="{{asset('img/rooms/'.$room->background)}}" alt="">
                            </td>
                            <td>{{ $room->price }}</td>
                            <td>{{ $room->status == 0 ? 'Available' : 'Booking' }}</td>
                            <td>
                                <a class="btn btn-info" href="{{ route('admin.rooms.edit', $room->id) }}">Detail</a>
                                <a onclick = "return confirm('Want to delete?')" class="btn btn-danger" href="{{ route('admin.rooms.destroy', $room->id) }}">Delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $rooms->appends(request()->all())->links() }}
            </div>
        </div>
    </div>
@endsection
    @if(session()->has('success'))
        @section('js')
            <script>
                Toastify({
                text: "{{ session()->get('success') }}",
                style: {
                    background: "linear-gradient(to right, #00b09b, #96c93d)",
                },
                duration: 3000
                }).showToast();
            </script>
        @endsection
    @endif

    @if(session()->has('error'))
        @section('js')
            <script>
                Toastify({
                text: "{{ session()->get('error') }}",
                style: {
                    background: "linear-gradient(to right, #00b09b, #96c93d)",
                },
                duration: 3000
                }).showToast();
            </script>
        @endsection
    @endif

@section('js')
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable({
                searching: false, paging: false, info: false
            });
        });
    </script>
@endsection