@extends('layouts.admin.master')
@section('content')


<div class="card shadow mb-4">
    <div class="card-header py-3">
        <form action="{{route('admin.dashboards.export-booking')}}" method="post" class="float-right">
            @csrf
            <input type="submit" value="Export CSV" name="export_csvBooking" class="btn btn-success">
        </form>
        <h6 class="m-0 font-weight-bold text-primary">REVENUE BY BOOKING</h6>
    </div>
    <div class="card-body">
        <div class="form-group">
            <form action="" class="form-inline mb-3">
                <input class="form-control" name="date" placeholder="Date ..." style="width:10%; margin-right:5px;">
                <input class="form-control" name="month" placeholder="Month ..." style="width:10%; margin-right:5px;">
                <input class="form-control" name="year" placeholder="Year ..." style="width:10%; margin-right:5px;">
                <button type="submit" class="btn btn-primary" title="Filter" style="margin-right:5px;"> <i class="fa fa-filter"></i></button>
            </form>
        </div>    
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>BOOKING USER</th>
                        <th>HOTEL</th>
                        <th>ROOM</th>
                        <th>CHECK IN</th>
                        <th>CHECK OUT</th>
                        <th>PRICE</th>
                        <th>PROFIT</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $stt=1;
                        foreach($revenues as $values){
                    ?>
                    <tr>
                        <td>{{ $stt }}</td>
                        <td>{{ $values->userName }}</td>
                        <td>{{ $values->hotelName }}</td>
                        <td>{{ $values->roomName }}</td>
                        <td>{{ $values->check_in }}</td>
                        <td>{{ $values->check_out }}</td>
                        <td>{{ $values->total }}</td>
                        <td>{{ isset($values->url) ? ($values->total)/100*20 : ($values->total)/100*30 }}</td>
                    </tr>
                    <?php
                        $stt++;
                    }
                    ?>
                </tbody>
            </table>
            {{ $revenues->appends(request()->all())->links() }}
        </div>
    </div>
</div>

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <form action="{{route('admin.dashboards.export-month')}}" method="post" class="float-right">
            @csrf
            <input type="submit" value="Export CSV" name="export_csvMonth" class="btn btn-success">
        </form>
        <h6 class="m-0 font-weight-bold text-primary">REVENUE IN THE LAST 6 MONTHS</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>MONTH</th>
                        @foreach($revenueMonth as $month)
                            <th> {{\Carbon\Carbon::parse($month->check_in)->format('m/Y')}} </th>
                        @endforeach                      
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>TURNOVER</td>
                        @foreach($revenueMonth as $month)
                            <th> {{ $month->sumMonth }} </th>
                        @endforeach   
                    </tr>
                   
                </tbody>  
            </table>
            
        </div>
    </div>
</div>

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <form action="{{route('admin.dashboards.export-year')}}" method="post" class="float-right">
            @csrf
            <input type="submit" value="Export CSV" name="export_csvYear" class="btn btn-success">
        </form>
        <h6 class="m-0 font-weight-bold text-primary">REVENUE IN THE LAST 5 YEARS</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>YEARS</th>
                        @foreach($revenueYear as $year)
                            <th> {{$year->year}} </th>
                        @endforeach 
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>TURNOVER</td>
                        @foreach($revenueYear as $year)
                            <th> {{$year->sumYear}} </th>
                        @endforeach 
                    </tr>
                </tbody>
                
            </table>
            
        </div>
    </div>
</div>

@endsection
