@extends('layouts.admin.master')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <h4>
                    <h1 class="text-center">Detail {{$hotel->name}} Hotel</h1>
                    <div class="card-body">
                        <h5>Manager : <b>{{$detailHotel['0']->name}}</b><br></h5>
                        <h5>View: <b>{{$detailHotel['0']->view}}</b><br></h5>
                        <h5>Background <img src="{{asset('img/hotel/'.$hotel->background)}}" width="100%" height="500px" style="margin-bottom: 5px"><br>
                        <h5>Address: <b>{{$hotel->address}}</b><br></h5>
                        <h5>Hotel Image 
                        <div class="row">
                            <div class="col-12">
                            @foreach(json_decode($hotel->images, true) as $value)
                                <img src="{{asset('img/hotel/'.$value)}}" width="100px" height="80px">
                            @endforeach
                            </div>
                        </div></h5> 
                    </div>
                </h4>           
            </div>
            <div class="text-right">
                <a href="{{ route('admin.hotel.index') }}" class="btn btn-danger float-end">Back</a>      
            </div>
        </div>
    </div>
</div>

@endsection