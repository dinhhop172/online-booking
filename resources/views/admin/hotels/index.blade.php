@extends('layouts.admin.master')
@section('content')

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">LIST HOTELS</h6>
        </div>
        <div class="card-body">
            <form action="" class="form-inline mb-4 justify-content-end">
                <div class="form-group">
                    <input class="form-control" name="key" placeholder="Search for...">
                </div>
                <button type="submit" class="btn btn-primary"><i class="fas fa-search fa-sm"></i></button>
            </form>
            <div class="table-responsive">
                @if (Session::has('success'))
                    <div class="alert alert-success" role="alert">
                        {{ Session::get('success') }}
                    </div>
                @endif
                @if (Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        {{ Session::get('error') }}
                    </div>
                @endif
                <table class="table table-bordered" id="tablehotel" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Name Hotel</th>
                            <th>Address</th>
                            <th>Background</th>
                            <th  class="text-right">Acction</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $stt=1;
                            foreach($hotels as $values){
                        ?>
                        <tr>
                            <td>{{ $stt }}</td>
                            <td>{{ $values->name }}</td>
                            <td>{{ $values->address }}</td>
                            <td><img src="{{asset('img/hotel/'.$values->background)}}" width="100px" height="70px"></td>
                            <td class="text-right">
                                <form action="{{route('admin.hotel.destroy', $values->id)}}" method="POST">
                                    <a class="btn btn-info" href="{{ route('admin.hotel.show',$values->id) }}"><i class="fas fa-info-circle"></i></a>
                                    @csrf
                                    @method('DELETE')
                                    <button onclick = "return confirm('Want to delete?')" type="submit" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                                </form>
                            </td> 
                        </tr>
                        <?php
                            $stt++;
                        }
                        ?>
                    </tbody>
                    
                </table>
            </div>
            <div class="text-right mt-4">
                {{ $hotels->appends(request()->all())->links() }}
            </div>
        </div>
    </div>
@endsection