@extends('layouts.admin.master')
@section('content')

    <div class="col-md-6 offset-md-4">
        <h3>CREATE USERS</h3>
    </div>
    @if (!isset($user))
    {!! Form::open(['route' => 'users.store', 'method'=>'POST']) !!}
    @else
    {!! Form::open(['route' => ['users.update',$user->id], 'method'=>'PUT']) !!}
    @endif
    <div class="form-group">
        {!! Form::text('name', isset($user)?$user->name:'', ['class'=>'form-control form-control-user', 'placeholder'=>'Full Name']) !!}
    </div>
    <div class="form-group">
        {!! Form::text('email', isset($user)?$user->email:'', ['class'=>'form-control form-control-user', 'placeholder'=>'Email']) !!}
    </div>
    <div class="form-group row">
        <div class="col-sm-6 mb-3 mb-sm-0">
             {!! Form::text('password',"", ['class'=>'form-control form-control-user', 'placeholder'=>'Password']) !!}
        </div>
        <div class="col-sm-6">
            {!! Form::text('repassword',"", ['class'=>'form-control form-control-user', 'placeholder'=>'Repeat Password']) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::select('role', ['1'=>'Admin', '2'=> 'User'], isset($user)?$user->role:'' , ['class'=>'form-control']) !!}
    </div>
    <div class="row">
        @if (!isset($user))
            {!! Form::submit('Thêm User', ['class'=>'col-md-6 offset-md-3 btn btn-primary btn-user btn-block']) !!}
        @else
            {!! Form::submit('Cập nhật', ['class'=>'col-md-6 offset-md-3 btn btn-primary btn-user btn-block']) !!}
        @endif
    </div>
    {!! Form::close() !!}
    <hr>
{{-- </div> --}}
@endsection
