@extends('layouts.admin.master')
@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">List Users</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered " id="usertable" width="100%">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">NAME</th>
                        <th scope="col">EMAIL</th>
                        {{-- <th scope="col">PASSWORD</th> --}}
                        <th scope="col">ROLE</th>
                        <th scope="col">PHONE</th>
                        <th scope="col">ADDRESS</th>
                        <th scope="col">GENDER</th>
                        <th scope="col">BIRTHDAY</th>
                        <th scope="col">IS_AFFILIATOR</th>
                        <th scope="col">REFEREN_URL</th>
                        <th scope="col">AFFILIATOR_REF</th>
                        <th scope="col">MONEY</th>
                        <th scope="col">STATUS</th>
                        <th scope="col" style="width:150px">DISABLE | ENABLE</th>
                        <th scope="col">MANAGE</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($list as $key => $user )
                    <tr>
                        <td scope="row">{{$key+1}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        {{-- <td>PASSWORD</td> --}}
                        <td>
                          @if ($user->role == 1)
                              Admin
                          @elseif ($user->is_affiliator == 1)
                              Affiliator
                          @else
                              User
                          @endif
                        </td>
                        <td>{{$user->phone}}</td>
                        <td>{{$user->address}}</td>
                        <td>{{$user->gender==0?'Male':'Famale'}}</td>
                        <td>{{$user->birthday}}</td>
                        <td>{{$user->is_affiliator == 1?'Yes':'No'}}</td>
                        <td>{{$user->referen_url}}</td>
                        <td>{{$user->affiliator_ref}}</td>
                        <td>{{$user->money}}</td>
                        <td>{{$user->status==0?'Enable':'Active'}}</td>
                        <td>
                             <a href="{{route('users-change_active',$user->id)}}"> {{$user->status==0?"ON ACTIVE":"OFF ACTIVE"}}</a>
                        </td>
                        <td style="display: flex; justify-content: space-between;width:180px">
                            <a href="{{route('users.show',$user->id)}}" class="btn btn-success">Detail</a>
                            {!! Form::open(['route'=>['users.destroy',$user->id], 'method'=>'DELETE', 'onsubmit'=>'return confirm("Bạn thực sự muốn xoá?")']) !!}
                            {!! Form::submit('Xoá', ['class'=>'btn btn-danger']) !!}
                            {!! Form::close() !!}
                            <a href="{{route('users.edit',$user->id)}}" class="btn btn-warning">Edit</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>

            </table>
        </div>
    </div>
</div>
@endsection

