@extends('layouts.admin.master')
@section('content')
<div class="row">
    <div class="col-md-6 offset-md-1">
        <h3>THÔNG TIN CHI TIẾT</h3>
    </div>
    <div class="col-md-6 offset-md-3">
       @if (isset($user->avatar))
            <img src="{{asset('img/users/'.$user->avatar)}}" width="40%" heigth="40%" alt="">
        @else
            <img src="{{asset('img/users/avatar.png')}}" width="40%" heigth="40%" alt="">
       @endif
    </div>

    <div class="col-md-6 offset-md-1">
        <hr>
        <h5>HỌ VÀ TÊN: {{$user->name}}</h5>
    </div>
    <div class="col-md-6 offset-md-1">
        <h5>EMAIL {{$user->email}}</h5>
    </div>
    <div class="col-md-6 offset-md-1">
        <h5>CHỨC VỤ:
            @if ($user->role == 1)
                ADMIN
            @elseif ($user->is_affiliator == 1)
                Affiliator
            @else
                User
            @endif
        </h5>
    </div>
    <div class="col-md-6 offset-md-1">
        <h5>PHONE: {{$user->phone}}</h5>
    </div>
    <div class="col-md-6 offset-md-1">
        <h5>STATUS: {{$user->status==1?'Đã kích hoạt':'Chưa kích hoạt'}}</h5>
    </div>
    <div class="col-md-6 offset-md-1" style="display: flex; justify-content: space-between">
        <a href="{{route('users.show',$user->id)}}" class="btn btn-success">Detail</a>
        {!! Form::open(['route'=>['users.destroy',$user->id], 'method'=>'DELETE', 'onsubmit'=>'return confirm("Bạn thực sự muốn xoá?")']) !!}
        {!! Form::submit('Xoá', ['class'=>'btn btn-danger']) !!}
        {!! Form::close() !!}
        <a href="{{route('users.edit',$user->id)}}" class="btn btn-warning">Edit</a>
    </div>

</div>
@endsection
