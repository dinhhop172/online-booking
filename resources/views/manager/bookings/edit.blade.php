@extends('layouts.admin.master')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Detail</h6>
                </div>
                <div class="card-body">
                    <h5 class="card-title"><strong>Manager: </strong> {{ $booking->user->name }}</h5>
                    <h5 class="card-title"><strong>Room: </strong> {{ $booking->room->name }}</h5>
                    <h5 class="card-title"><strong>Check in: </strong> {{ $booking->check_in }}</h5>
                    <h5 class="card-title"><strong>Check out: </strong> {{ $booking->check_out }}</h5>
                    <h5 class="card-title"><strong>Total: </strong> {{ $booking->total }}</h5>
                    <h5 class="card-title"><strong>Status: </strong> 
                            @if($booking->status == App\Enums\BookingStatus::SCHEDULE)
                                {{'Schedule'}}
                            @elseif($booking->status == App\Enums\BookingStatus::DELIVERY)
                                {{'Delivery'}}
                            @elseif($booking->status == App\Enums\BookingStatus::PAID)
                                {{'Paid'}}
                            @elseif($booking->status == App\Enums\BookingStatus::DONE)
                                {{'Done'}}
                            @elseif($booking->status == App\Enums\BookingStatus::CANCEL)
                                {{'Cancel'}}
                            @endif
                    </h5>
                    <h5 class="card-title"><strong>Payment status: </strong> {{ $booking->payment_status == App\Enums\PayStatus::UNPAID ? 'Not payment' : 'Paid'}}</h5>
                    <h5 class="card-title"><strong>Note: </strong> {{ $booking->note ?? 'None'}}</h5>
                    
                    <div class="row ml-1 mt-2">
                        <a href="{{ route('manager.bookings.index') }}" class="btn btn-danger">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection