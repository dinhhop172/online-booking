@extends('layouts.admin.master')
@section('content')

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">LIST MANAGER</h6>
        </div>
        <div class="row">
            
            <form method="GET" class="pt-3 d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                <div class="input-group">
                    <input type="text" name="key" class="form-control bg-light border-0 small" placeholder="Search for..."
                        aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit">
                            <i class="fas fa-search fa-sm"></i>
                        </button>
                    </div>
                </div>
            </form>
            <form method="GET" class="pt-3 d-none d-sm-inline-block form-inline mr-4 ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                <div class="input-group">
                    <input type="date" name="check_in" class="form-control bg-light border-0 small" />
                    <input type="date" name="check_out" class="form-control bg-light border-0 small" />
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit">
                            <i class="fas fa-search fa-sm"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="tablehotel" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>*</th>
                            <th>User</th>
                            <th>Room</th>
                            <th>Check in</th>
                            <th>Check out</th>
                            <th>Payment Status</th>
                            <th class="text-right">Acction</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $i = 1;
                        @endphp
                        @foreach($bookings as $booking)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $booking->user->name }}</td>
                            <td>{{ $booking->room->name }}</td>
                            <td>{{ $booking->check_in }}</td>
                            <td>{{ $booking->check_out }}</td>
                            <td>{{ ($booking->payment_status == \App\Enums\PayStatus::UNPAID) ? 'Not payment' : 'Paid' }}</td>
                            <td>
                                <a class="btn btn-info" href="{{ route('manager.bookings.edit', $booking->id) }}"><i class="fas fa-info-circle"></i></a>
                                <a onclick = "return confirm('Want to delete?')" class="btn btn-danger" href="{{ route('manager.bookings.destroy', $booking->id) }}"><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    
                </table>
            </div>
        </div>
        <div class="col-xs-6 col-md-4">
            {{ $bookings->appends(request()->all())->links() }}
        </div>
    </div>
@endsection

@if(session()->has('success'))
    @section('js')
        <script>
            Toastify({
            text: "{{ session()->get('success') }}",
            style: {
                background: "linear-gradient(to right, #00b09b, #96c93d)",
            },
            duration: 3000
            }).showToast();
        </script>
    @endsection
@endif

@if(session()->has('error'))
    @section('js')
        <script>
            Toastify({
            text: "{{ session()->get('error') }}",
            style: {
                background: "linear-gradient(to right, #00b09b, #96c93d)",
            },
            duration: 3000
            }).showToast();
        </script>
    @endsection
@endif