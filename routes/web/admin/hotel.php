<?php

use App\Http\Controllers\admin\HotelController;

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin.auth'], function () {
    Route::resource('hotel', HotelController::class);
});

?>