<?php

use App\Http\Controllers\manager\BookingController;
use App\Models\Booking;

Route::group(['prefix' => 'manager/bookings', 'as'=>'manager.bookings.', 'middleware' => 'admin.auth'], function () {
    Route::get('/', [BookingController::class, 'index'])->name('index');
    Route::get('/{id}/edit', [BookingController::class, 'edit'])->name('edit');
    Route::get('/{id}/delete', [BookingController::class, 'destroy'])->name('destroy');
    Route::get('as', function(){
        $name = 'ho';
        dd(Booking::whereHas('user', function($q) use($name) {
            $q->where('name', 'like', '%' . $name . '%');
        })->get());
    });
});

?>