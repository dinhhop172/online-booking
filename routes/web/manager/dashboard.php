<?php

use App\Http\Controllers\manager\DashBoardController;

Route::group(['prefix' => 'manager/dashboards', 'as' => 'manager.dashboards.', 'middleware' => 'admin.auth'], function () {
    Route::get('/', [DashBoardController::class, 'index'])->name('index');
});

?>