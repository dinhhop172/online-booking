<?php

use App\Http\Controllers\auth\ChangePasswordController;
use App\Http\Controllers\auth\ResetPasswordController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/forgot-password', [ResetPasswordController::class, 'index'])->name('forgot.password');
Route::post('/forgot-password', [ResetPasswordController::class, 'sendResetLink']);
Route::get('/reset-password/{email}/{token}', [ResetPasswordController::class, 'showResetPasswordForm'])->name('reset.password');
Route::post('/reset-password', [ResetPasswordController::class, 'submitResetPasswordForm'])->name('reset.password.post');

Route::middleware('auth')->prefix('admin')->group(function () {
    Route::get('/change-password', [ChangePasswordController::class, 'index'])->name('change.password');
    Route::post('/change-password', [ChangePasswordController::class, 'submitChangePassword']);
});

//admin
require __DIR__ . '/web/admin/booking.php';
require __DIR__ . '/web/admin/category.php';
require __DIR__ . '/web/admin/dashboard.php';
require __DIR__ . '/web/admin/hotel.php';
require __DIR__ . '/web/admin/rating.php';
require __DIR__ . '/web/admin/request_payment.php';
require __DIR__ . '/web/admin/revenue.php';
require __DIR__ . '/web/admin/room.php';
require __DIR__ . '/web/admin/user.php';

//auth
require __DIR__ . '/web/auth/login_admin.php';
require __DIR__ . '/web/auth/login_manager.php';
require __DIR__ . '/web/auth/login_user.php';
require __DIR__ . '/web/auth/register_user.php';

//home affiliator
require __DIR__ . '/web/home/affiliator/profile.php';
require __DIR__ . '/web/home/affiliator/request_payment.php';
require __DIR__ . '/web/home/affiliator/revenue.php';

//home tourist
require __DIR__ . '/web/home/tourist/booking.php';
require __DIR__ . '/web/home/tourist/homepage.php';
require __DIR__ . '/web/home/tourist/profile.php';
require __DIR__ . '/web/home/tourist/rating.php';

//manager
require __DIR__ . '/web/manager/dashboard.php';
require __DIR__ . '/web/manager/booking.php';
require __DIR__ . '/web/manager/profile.php';
require __DIR__ . '/web/manager/rating.php';
require __DIR__ . '/web/manager/revenue.php';
require __DIR__ . '/web/manager/room.php';
