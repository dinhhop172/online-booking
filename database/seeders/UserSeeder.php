<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Nguyễn Văn Hoài',
            'email' => 'nguyenvanhoai1280@gmail.com',
            'password' => Hash::make('admin123'),
            'role' => 0,
            'status' => 1,
        ]);
        User::create([
            'name' => 'Nguyễn Hoàng Minh',
            'email' => 'hoangminh220498@gmail.com',
            'password' => Hash::make('admin123'),
            'role' => 0,
            'status' => 1,
        ]);
        User::create([
            'name' => 'Hoàng Đình Hợp',
            'email' => 'dinhhop172@gmail.com',
            'password' => Hash::make('admin123'),
            'role' => 0,
            'status' => 1,
        ]);
        User::create([
            'name' => 'Nguyễn Quang Đức',
            'email' => 'duciker14@gmail.com',
            'password' => Hash::make('admin123'),
            'role' => 0,
            'status' => 1,
        ]);
        User::create([
            'name' => 'Lương Minh Dương',
            'email' => 'duong40541@gmail.com',
            'password' => Hash::make('admin123'),
            'role' => 0,
            'status' => 1,
        ]);
        User::factory()->count(10)->create();
    }
}
