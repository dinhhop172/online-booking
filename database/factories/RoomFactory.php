<?php

namespace Database\Factories;

use App\Models\Hotel;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class RoomFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::all()->random()->id,
            'hotel_id' => Hotel::all()->random()->id,
            'name' => $this->faker->name(),
            'img'=>'{"0":"anh1.jpg","1":"anh2.jpg","2":"anh3.jpg","3":"anh4.jpg"}',
            'background'=> 'anh1.jpg',
            'price' => $this->faker->numberBetween(100, 1000),
            'status' => $this->faker->numberBetween(0, 1),
        ];
    }
}
