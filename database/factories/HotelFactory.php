<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class HotelFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => 1,
            'name' => $this->faker->name(),
            'address' => $this->faker->address(),
            'background'=> 'lasiden.png',
            'images'=>'{"img1":"img1.png","img2":"img2.png","img3":"img3.png","img4":"img4.png","img5":"img5.png","img6":"img6.png","img7":"img7.png","img8":"img8.png","img9":"img9.png","img10":"img10.png"}',
        ];
    }
}
